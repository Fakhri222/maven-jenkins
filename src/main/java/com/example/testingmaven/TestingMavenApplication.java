package com.example.testingmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestingMavenApplication.class, args);
    }

}
